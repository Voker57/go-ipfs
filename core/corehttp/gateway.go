package corehttp

import (
	"fmt"
	"net"
	"net/http"

	core "github.com/ipfs/go-ipfs/core"
	coreapi "github.com/ipfs/go-ipfs/core/coreapi"
	config "github.com/ipfs/go-ipfs/repo/config"

	id "gx/ipfs/Qma23bpHwQrQyvKeBemaeJh7sAoRHggPkgnge1B9489ff5/go-libp2p/p2p/protocol/identify"
)

type GatewayConfig struct {
	Headers      map[string][]string
	Writable     bool
	FetchBlocks  bool
	PathPrefixes []string
}

func ConfigFromNode(n *core.IpfsNode) (*GatewayConfig, error) {
	cfg, err := n.Repo.Config()
	if err != nil {
		return nil, err
	}
	gc := &GatewayConfig{
		Headers:      cfg.Gateway.HTTPHeaders,
		PathPrefixes: cfg.Gateway.PathPrefixes,
		FetchBlocks:  cfg.Gateway.FetchBlocks,
	}
	return gc, nil
}

func GatewayOption(gc GatewayConfig, paths ...string) ServeOption {
	return func(n *core.IpfsNode, _ net.Listener, mux *http.ServeMux) (*http.ServeMux, error) {
		gateway := newGatewayHandler(n, gc, coreapi.NewCoreAPI(n, !gc.FetchBlocks))

		for _, p := range paths {
			mux.Handle(p+"/", gateway)
		}
		return mux, nil
	}
}

func VersionOption() ServeOption {
	return func(_ *core.IpfsNode, _ net.Listener, mux *http.ServeMux) (*http.ServeMux, error) {
		mux.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Commit: %s\n", config.CurrentCommit)
			fmt.Fprintf(w, "Client Version: %s\n", id.ClientVersion)
			fmt.Fprintf(w, "Protocol Version: %s\n", id.LibP2PVersion)
		})
		return mux, nil
	}
}
